variable "ami" {
 description = "the AMI to use"
}
variable "access_key" {
 description = "the AMI to use"
}
variable "secret_key" {
 description = "the AMI to use"
}
provider "aws" {
 access_key = "${var.access_key}"
 secret_key = "${var.secret_key}"
 region = "us-east-1"
}
resource "aws_instance" "example" {
 ami = "${var.ami}"
 instance_type = "t2.micro"
}
terraform {
 backend "s3" {
 bucket = "esr-devops-tfstate"
 key = "state/terraform.tfstate"
 region = "us-east-1"
 }
}