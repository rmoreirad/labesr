def test_nginx_socket_listening(host):
    nginx = host.socket('TCP://0.0.0.0:80')
    assert nginx.is_listening